import React from 'react'
import {Button} from './Button'
import './HeroSection.css'

function HeroSection() {
    return (
        <div className='hero-container'>
            <video src="/videos/video-1.mp4" autoPlay loop muted />
            <h1>Bem vindo</h1>
            <p>Oque você precisa ?</p>
            <div className="hero-btns">
                <Button className="btns"
                        buttonStyle='btn--outline'
                        buttonSize='btn--large'>
                            GET STARTED
                </Button>

                <Button className="btns"
                        buttonStyle='btn--primary'
                        buttonSize='btn--large'>
                            WHATCH TRAILER
                        <i className='far fa-play-circle'/>
                </Button>
            </div>
        </div>
    );
}

export default HeroSection;
