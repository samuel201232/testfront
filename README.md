Escreva uma aplicação usando React e as bibliotecas da sua escolha, para construir a seguinte cena: Um prédio com 12 janelas. Ao clicar nas janelas o usuário liga e desliga as luzes das janelas. A cena também contém um controle com um botão que permite e ligar e desligar todas as janelas. Use a Sunset Sunrise API (https://sunrise-sunset.org/api) para mudar automaticamente a cena dependendo se está noite ou dia no seu local. Ou seja, utilize API do browser de geolocalização.


Funcionalidades
Ligar e desligar as luzes ao clicar nas janelas;
Alterar cenário para dia e noite ao clicar no sol ou lua;
Indentificar a localização do usuário através do browser;
Pegar latitude e longitude.



Tecnologias estudadas
React
TypeScript
Styled Components
axios
Sunrise Sunset API
